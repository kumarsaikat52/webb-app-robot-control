/**
 * @author mrdoob / http://mrdoob.com/
 */

Menubar.File = function (editor) {



	var NUMBER_PRECISION = 6;

	function parseNumber(key, value) {

		return typeof value === 'number' ? parseFloat(value.toFixed(NUMBER_PRECISION)) : value;

	}

	//

	var config = editor.config;
	var strings = editor.strings;
	var osInfo = editor.osInfo;

	var container = new UI.Panel();
	container.setClass('menu');

	var title = new UI.Panel();
	title.setClass('title');
	title.setTextContent(strings.getKey('menubar/file'));
	container.add(title);

	var options = new UI.Panel();
	options.setClass('options');
	container.add(options);

	// New

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/new'));
	option.onClick(function () {

		if (confirm('Any unsaved data will be lost. Are you sure?')) {

			editor.clear();

		}

	});
	options.add(option);

	//

	options.add(new UI.HorizontalRule());

	// Import Model

	var form = document.createElement('form');
	form.style.display = 'none';
	document.body.appendChild(form);

	var fileInput = document.createElement('input');
	fileInput.multiple = true;
	fileInput.type = 'file';
	fileInput.addEventListener('change', function (event) {

		var extension = fileInput.value.split('.').pop().toLowerCase();
		console.log(extension);
		if (extension == "json") {
			alert("This is not a model Format");
			return;
		}
		editor.loader.loadFiles(fileInput.files);
		//console.log(fileInput.value);


		form.reset();

	});
	form.appendChild(fileInput);

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/importModel'));
	option.onClick(function () {

		fileInput.click();

	});
	options.add(option);

	// Import Scene

	var form2 = document.createElement('form');
	form2.style.display = 'none';
	document.body.appendChild(form2);

	var fileInput2 = document.createElement('input');
	fileInput2.multiple = false;
	fileInput2.type = 'file';
	fileInput2.addEventListener('change', function (event) {

		var extension = fileInput2.value.split('.').pop().toLowerCase();
		console.log(extension);
		if (extension != "json") {
			alert("This is not a Scene Format. Load .json file format.");
			return;
		}
		editor.loader.loadFiles(fileInput2.files);
		//console.log(fileInput.value);


		form.reset();

	});
	form2.appendChild(fileInput2);

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/importJson'));
	option.onClick(function () {

		fileInput2.click();

	});
	options.add(option);

	///Import waypoints

	var form2 = document.createElement('form');
	form2.style.display = 'none';
	document.body.appendChild(form2);

	var fileInput2 = document.createElement('input');
	fileInput2.multiple = false;
	fileInput2.type = 'file';
	fileInput2.addEventListener('change', function (event) {


		var extension = fileInput2.value.split('.').pop().toLowerCase();
		console.log(extension);
		if (extension != "json") {
			alert("This is not a Scene Format. Load .json file format.");
			return;
		}
		//console.log(editor.loader.loadFiles(fileInput2.files));
		// $.getJSON(fileInput2.value, function(json) {
		// 	console.log(json); // this will show the info it in firebug console
		// });

		editor.clear();
		editor.loader.loadFiles(fileInput2.files);




		form.reset();

	});
	form2.appendChild(fileInput2);

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/importWayPoints'));
	option.onClick(function () {

		fileInput2.click();

	});
	options.add(option);

	//import custom file at the start

	// var loc = window.location.pathname;
	// var dir = loc.substring(0, loc.lastIndexOf('/'));
	// dir = dir + osInfo.ModelLocation + "straight_tube_2.obj";
	// var s = dir.substring(dir.lastIndexOf('/')+1);
	// console.log(s);
	// var obj = {};
	// obj.name = s;

	//editor.loader.loadFileDirectly(obj);

	options.add(new UI.HorizontalRule());

	// Export Geometry

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/geometry' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null ) {

	// 		alert( 'No object selected.' );
	// 		return;

	// 	}

	// 	var geometry = object.geometry;

	// 	if ( geometry === undefined ) {

	// 		alert( 'The selected object doesn\'t have geometry.' );
	// 		return;

	// 	}

	// 	var output = geometry.toJSON();

	// 	try {

	// 		output = JSON.stringify( output, parseNumber, '\t' );
	// 		output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

	// 	} catch ( e ) {

	// 		output = JSON.stringify( output );

	// 	}

	// 	saveString( output, 'geometry.json' );

	// } );
	// options.add( option );

	// // Export Object

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/object' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null ) {

	// 		alert( 'No object selected' );
	// 		return;

	// 	}

	// 	var output = object.toJSON();

	// 	try {

	// 		output = JSON.stringify( output, parseNumber, '\t' );
	// 		output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

	// 	} catch ( e ) {

	// 		output = JSON.stringify( output );

	// 	}

	// 	saveString( output, 'model.json' );

	// } );
	// options.add( option );

	// Export Scene

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/export/scene'));
	option.onClick(function () {

		var output = editor.scene.toJSON();

		try {

			output = JSON.stringify(output, parseNumber, '\t');
			output = output.replace(/[\n\t]+([\d\.e\-\[\]]+)/g, '$1');

		} catch (e) {

			output = JSON.stringify(output);

		}

		saveString(output, 'scene.json');

	});
	options.add(option);

	// Export waypoints

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/export/waypoints'));
	option.onClick(function () {

		var positions = [];

		editor.waypoints.forEach(element => {

			positions.push(element.position);

		});
		var metadata = {};
		metadata.type = 'waypoint';
		metadata.generator = 'Custom';

		var jsonData = {};
		jsonData.metadata = metadata;
		jsonData.positions = positions
		var output = JSON.stringify(jsonData);

		try {

			output = JSON.stringify(JSON.parse(output), null, 4);
			//output = JSON.stringify(output, null, '\t');
			//output = output.replace(/[\n\t]+([\d\.e\-\[\]]+)/g, '$1');

		} catch (e) {

			output = JSON.stringify(output);

		}

		saveString(output, 'Custom waypoints.json');

	});
	options.add(option);

	// //

	// options.add( new UI.HorizontalRule() );

	// // Export DAE

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/dae' ) );
	// option.onClick( function () {

	// 	var exporter = new THREE.ColladaExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveString( result.data, 'scene.dae' );

	// 	} );

	// } );
	// options.add( option );

	// // Export GLB

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/glb' ) );
	// option.onClick( function () {

	// 	var exporter = new THREE.GLTFExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveArrayBuffer( result, 'scene.glb' );

	// 		// forceIndices: true, forcePowerOfTwoTextures: true
	// 		// to allow compatibility with facebook
	// 	}, { binary: true, forceIndices: true, forcePowerOfTwoTextures: true } );

	// } );
	// options.add( option );

	// // Export GLTF

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/gltf' ) );
	// option.onClick( function () {

	// 	var exporter = new THREE.GLTFExporter();

	// 	exporter.parse( editor.scene, function ( result ) {

	// 		saveString( JSON.stringify( result, null, 2 ), 'scene.gltf' );

	// 	} );


	// } );
	// options.add( option );

	// // Export OBJ

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/obj' ) );
	// option.onClick( function () {

	// 	var object = editor.selected;

	// 	if ( object === null ) {

	// 		alert( 'No object selected.' );
	// 		return;

	// 	}

	// 	var exporter = new THREE.OBJExporter();

	// 	saveString( exporter.parse( object ), 'model.obj' );

	// } );
	// options.add( option );

	// // Export STL (ASCII)

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/stl' ) );
	// option.onClick( function () {

	// 	var exporter = new THREE.STLExporter();

	// 	saveString( exporter.parse( editor.scene ), 'model.stl' );

	// } );
	// options.add( option );

	// // Export STL (Binary)

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/export/stl_binary' ) );
	// option.onClick( function () {

	// 	var exporter = new THREE.STLExporter();

	// 	saveArrayBuffer( exporter.parse( editor.scene, { binary: true } ), 'model-binary.stl' );

	// } );
	// options.add( option );

	// //

	// options.add( new UI.HorizontalRule() );

	// Publish

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/file/publish' ) );
	// option.onClick( function () {

	// 	var zip = new JSZip();

	// 	//

	// 	var output = editor.toJSON();
	// 	output.metadata.type = 'App';
	// 	delete output.history;

	// 	var vr = output.project.vr;

	// 	output = JSON.stringify( output, parseNumber, '\t' );
	// 	output = output.replace( /[\n\t]+([\d\.e\-\[\]]+)/g, '$1' );

	// 	zip.file( 'app.json', output );

	// 	//

	// 	var title = config.getKey( 'project/title' );

	// 	var manager = new THREE.LoadingManager( function () {

	// 		save( zip.generate( { type: 'blob' } ), ( title !== '' ? title : 'untitled' ) + '.zip' );

	// 	} );

	// 	var loader = new THREE.FileLoader( manager );
	// 	loader.load( 'js/libs/app/index.html', function ( content ) {

	// 		content = content.replace( '<!-- title -->', title );

	// 		var includes = [];

	// 		if ( vr ) {

	// 			includes.push( '<script src="js/WebVR.js"></script>' );

	// 		}

	// 		content = content.replace( '<!-- includes -->', includes.join( '\n\t\t' ) );

	// 		var editButton = '';

	// 		if ( config.getKey( 'project/editable' ) ) {

	// 			editButton = [
	// 				'',
	// 				'			var button = document.createElement( \'a\' );',
	// 				'			button.href = \'https://threejs.org/editor/#file=\' + location.href.split( \'/\' ).slice( 0, - 1 ).join( \'/\' ) + \'/app.json\';',
	// 				'			button.style.cssText = \'position: absolute; bottom: 20px; right: 20px; padding: 12px 14px; color: #fff; border: 1px solid #fff; border-radius: 4px; text-decoration: none;\';',
	// 				'			button.target = \'_blank\';',
	// 				'			button.textContent = \'EDIT\';',
	// 				'			document.body.appendChild( button );',
	// 				''
	// 			].join( '\n' );
	// 		}

	// 		content = content.replace( '\n\t\t\t/* edit button */\n', editButton );

	// 		zip.file( 'index.html', content );

	// 	} );
	// 	loader.load( 'js/libs/app.js', function ( content ) {

	// 		zip.file( 'js/app.js', content );

	// 	} );
	// 	loader.load( '../build/three.min.js', function ( content ) {

	// 		zip.file( 'js/three.min.js', content );

	// 	} );

	// 	if ( vr ) {

	// 		loader.load( '../examples/js/vr/WebVR.js', function ( content ) {

	// 			zip.file( 'js/WebVR.js', content );

	// 		} );

	// 	}

	// } );
	// options.add( option );
	//Load waypoint
	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/file/export/waypoint'));
	option.onClick(function () {

		var object = editor.selected;

		if (object === null) {

			alert('Function not yet implemented');
			return;

		}
		alert('Function not yet implemented');
		//var exporter = new THREE.OBJExporter();

		//saveString(exporter.parse(object), 'model.obj');

	});
	//options.add(option);


	//

	var link = document.createElement('a');
	function save(blob, filename) {

		link.href = URL.createObjectURL(blob);
		link.download = filename || 'data.json';
		link.dispatchEvent(new MouseEvent('click'));

		// URL.revokeObjectURL( url ); breaks Firefox...

	}

	function saveArrayBuffer(buffer, filename) {

		save(new Blob([buffer], { type: 'application/octet-stream' }), filename);

	}

	function saveString(text, filename) {

		save(new Blob([text], { type: 'text/plain' }), filename);

	}

	return container;

};
