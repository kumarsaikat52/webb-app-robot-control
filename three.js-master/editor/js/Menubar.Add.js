//import { Vector3 } from "three";

/**
 * @author mrdoob / http://mrdoob.com/
 */
/*
* @editedby Kumar Halder
*/
Menubar.Add = function (editor) {
	editor.nothing = 5;
	var strings = editor.strings;
	var signals = editor.signals;
	var container = new UI.Panel();
	container.setClass('menu');

	var title = new UI.Panel();
	title.setClass('title');
	title.setTextContent(strings.getKey('menubar/add'));
	container.add(title);

	var options = new UI.Panel();
	options.setClass('options');
	container.add(options);

	//Line
	var waypoint_count = 0;
	var previousPoint = null;
	// Group

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/add/group'));
	option.onClick(function () {

		var mesh = new THREE.Group();
		mesh.name = 'Group';

		editor.execute(new AddObjectCommand(mesh));

	});
	options.add(option);

	//

	options.add(new UI.HorizontalRule());

	// // Plane

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/plane' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.PlaneBufferGeometry( 1, 1, 1, 1 );
	// 	var material = new THREE.MeshStandardMaterial();
	// 	var mesh = new THREE.Mesh( geometry, material );
	// 	mesh.name = 'Plane';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// Box

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/add/box'));
	option.onClick(function () {

		var geometry = new THREE.BoxBufferGeometry(1, 1, 1, 1, 1, 1);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = 'Box';

		editor.execute(new AddObjectCommand(mesh));

	});
	options.add(option);

	// // Circle

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/circle' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.CircleBufferGeometry( 1, 8, 0, Math.PI * 2 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Circle';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // Ring

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/ring' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.RingBufferGeometry( 0.5, 1, 8, 1, 0, Math.PI * 2 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Ring';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // Cylinder

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/cylinder' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.CylinderBufferGeometry( 1, 1, 1, 8, 1, false, 0, Math.PI * 2 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Cylinder';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// Sphere

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/sphere' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.SphereBufferGeometry( 1, 8, 6, 0, Math.PI * 2, 0, Math.PI );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Sphere';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // Icosahedron

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/icosahedron' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.IcosahedronBufferGeometry( 1, 0 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Icosahedron';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // Octahedron

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/octahedron' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.OctahedronBufferGeometry( 1, 0 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Octahedron';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// Tetrahedron

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/add/tetrahedron'));
	option.onClick(function () {

		addTetrahedron();

	});
	options.add(option);
	function addTetrahedron() {
		waypoint_count = waypoint_count + 1;
		var geometry = new THREE.TetrahedronBufferGeometry(1, 0);
		var mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial());
		mesh.name = 'Tetrahedron' + waypoint_count;
		mesh.line = [];
		editor.waypoints.push(mesh);
		editor.execute(new AddObjectCommand(mesh));
	}

	// // Torus

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/torus' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.TorusBufferGeometry( 1, 0.4, 8, 6, Math.PI * 2 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Torus';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // TorusKnot

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/torusknot' ) );
	// option.onClick( function () {

	// 	var geometry = new THREE.TorusKnotBufferGeometry( 1, 0.4, 64, 8, 2, 3 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'TorusKnot';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // Tube

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/tube' ) );
	// option.onClick( function () {

	// 	var path = new THREE.CatmullRomCurve3( [
	// 		new THREE.Vector3( 2, 2, - 2 ),
	// 		new THREE.Vector3( 2, - 2, - 0.6666666666666667 ),
	// 		new THREE.Vector3( - 2, - 2, 0.6666666666666667 ),
	// 		new THREE.Vector3( - 2, 2, 2 )
	// 	] );

	// 	var geometry = new THREE.TubeBufferGeometry( path, 64, 1, 8, false );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial() );
	// 	mesh.name = 'Tube';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// /*
	// // Teapot

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( 'Teapot' );
	// option.onClick( function () {

	// 	var size = 50;
	// 	var segments = 10;
	// 	var bottom = true;
	// 	var lid = true;
	// 	var body = true;
	// 	var fitLid = false;
	// 	var blinnScale = true;

	// 	var material = new THREE.MeshStandardMaterial();

	// 	var geometry = new THREE.TeapotBufferGeometry( size, segments, bottom, lid, body, fitLid, blinnScale );
	// 	var mesh = new THREE.Mesh( geometry, material );
	// 	mesh.name = 'Teapot';

	// 	editor.addObject( mesh );
	// 	editor.select( mesh );

	// } );
	// options.add( option );
	// */

	// // Lathe

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/lathe' ) );
	// option.onClick( function () {

	// 	var points = [
	// 		new THREE.Vector2( 0, 0 ),
	// 		new THREE.Vector2( 0.4, 0 ),
	// 		new THREE.Vector2( 0.35, 0.05 ),
	// 		new THREE.Vector2( 0.1, 0.075 ),
	// 		new THREE.Vector2( 0.08, 0.1 ),
	// 		new THREE.Vector2( 0.08, 0.4 ),
	// 		new THREE.Vector2( 0.1, 0.42 ),
	// 		new THREE.Vector2( 0.14, 0.48 ),
	// 		new THREE.Vector2( 0.2, 0.5 ),
	// 		new THREE.Vector2( 0.25, 0.54 ),
	// 		new THREE.Vector2( 0.3, 1.2 )
	// 	];

	// 	var geometry = new THREE.LatheBufferGeometry( points, 12, 0, Math.PI * 2 );
	// 	var mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial( { side: THREE.DoubleSide } ) );
	// 	mesh.name = 'Lathe';

	// 	editor.execute( new AddObjectCommand( mesh ) );

	// } );
	// options.add( option );

	// // Sprite

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/sprite' ) );
	// option.onClick( function () {

	// 	var sprite = new THREE.Sprite( new THREE.SpriteMaterial() );
	// 	sprite.name = 'Sprite';

	// 	editor.execute( new AddObjectCommand( sprite ) );

	// } );
	// options.add( option );

	// //

	// options.add( new UI.HorizontalRule() );

	// // PointLight

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/pointlight' ) );
	// option.onClick( function () {

	// 	var color = 0xffffff;
	// 	var intensity = 1;
	// 	var distance = 0;

	// 	var light = new THREE.PointLight( color, intensity, distance );
	// 	light.name = 'PointLight';

	// 	editor.execute( new AddObjectCommand( light ) );

	// } );
	// options.add( option );

	// SpotLight

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/add/spotlight'));
	option.onClick(function () {

		var color = 0xffffff;
		var intensity = 1;
		var distance = 0;
		var angle = Math.PI * 0.1;
		var penumbra = 0;

		var light = new THREE.SpotLight(color, intensity, distance, angle, penumbra);
		light.name = 'SpotLight';
		light.target.name = 'SpotLight Target';

		light.position.set(5, 10, 7.5);

		editor.execute(new AddObjectCommand(light));

	});
	options.add(option);

	// DirectionalLight

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/add/directionallight'));
	option.onClick(function () {

		var color = 0xffffff;
		var intensity = 1;

		var light = new THREE.DirectionalLight(color, intensity);
		light.name = 'DirectionalLight';
		light.target.name = 'DirectionalLight Target';

		light.position.set(5, 10, 7.5);

		editor.execute(new AddObjectCommand(light));

	});
	options.add(option);

	function addLightStartofScene() {
		var color = 0xffffff;
		var intensity = .5;

		var light = new THREE.DirectionalLight(color, intensity);
		light.name = 'DirectionalLight';
		light.target.name = 'DirectionalLight Target';
		light.position.set(5, 10, 7.5);
		editor.DirectionalLight = light;
		lightCopy = Object.create(light);

		var ambientlight = new THREE.AmbientLight(color, intensity = .7);
		ambientlight.name = 'ambient light';
		
		editor.ambientLight = ambientlight;
		ambientlightCopy = Object.create(ambientlight);

		editor.sceneHelpers.add(lightCopy);
		editor.sceneHelpers.add(ambientlightCopy);
		editor.scene.add(ambientlight);
		editor.scene.add(light);
		//editor.execute(new AddObjectCommand(light));
		//editor.execute(new AddObjectCommand(ambientlight));
	};

	addLightStartofScene();


	// HemisphereLight

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/hemispherelight' ) );
	// option.onClick( function () {

	// 	var skyColor = 0x00aaff;
	// 	var groundColor = 0xffaa00;
	// 	var intensity = 1;

	// 	var light = new THREE.HemisphereLight( skyColor, groundColor, intensity );
	// 	light.name = 'HemisphereLight';

	// 	light.position.set( 0, 10, 0 );

	// 	editor.execute( new AddObjectCommand( light ) );

	// } );
	// options.add( option );

	// AmbientLight

	var option = new UI.Row();
	option.setClass('option');
	option.setTextContent(strings.getKey('menubar/add/ambientlight'));
	option.onClick(function () {

		var color = 0x222222;

		var light = new THREE.AmbientLight(color);
		light.name = 'AmbientLight';

		editor.execute(new AddObjectCommand(light));

	});
	options.add(option);

	//

	//options.add( new UI.HorizontalRule() );

	// PerspectiveCamera

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/perspectivecamera' ) );
	// option.onClick( function () {

	// 	var camera = new THREE.PerspectiveCamera();
	// 	camera.name = 'PerspectiveCamera';

	// 	editor.execute( new AddObjectCommand( camera ) );

	// } );
	// options.add( option );

	// // OrthographicCamera

	// var option = new UI.Row();
	// option.setClass( 'option' );
	// option.setTextContent( strings.getKey( 'menubar/add/orthographiccamera' ) );
	// option.onClick( function () {

	// 	var camera = new THREE.OrthographicCamera();
	// 	camera.name = 'OrthographicCamera';

	// 	editor.execute( new AddObjectCommand( camera ) );

	// } );
	// options.add( option );

	///Add robot arm at the beginning of the scene
	var groupJ1 = new THREE.Group();
	var groupJ2 = new THREE.Group();
	var groupJ3 = new THREE.Group();
	var groupJ4 = new THREE.Group();
	var groupJ5 = new THREE.Group();
	var groupJ6 = new THREE.Group();
	editor.groupJ1 = groupJ1;
	editor.groupJ2 = groupJ2;
	editor.groupJ3 = groupJ3;
	editor.groupJ4 = groupJ4;
	editor.groupJ5 = groupJ5;
	editor.groupJ6 = groupJ6;
	//var scene = editor.sceneHelpers;
	//sceneHelpers.add(groupJ1);
	groupJ1.position.x = 0;
	groupJ1.position.y = 0;
	groupJ1.position.z = 0;

	function instantiateRobotArmComponenst() {
		var loader = new THREE.OBJLoader();
		var part1;




		///Aubo arm assembly 
		materialArm = new THREE.MeshPhongMaterial({ color: "rgb(255,100,0)" });
		materialArm.transparent = true;
		materialArm.opacity = 1;
		loader.load('http://127.0.0.1:8080/large_module_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = 0;
			object.position.y = 0;
			object.position.z = 0;
			object.rotateX(THREE.Math.degToRad(90));
			object.rotateY(THREE.Math.degToRad(180));
			object.rotateZ(THREE.Math.degToRad(0));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);

			part1 = object;
			part1.name = 'part1';
			groupJ1.add(part1);
			//scene.add(groupJ1);
			groupJ1.name = 'joint1';
			//editor.execute(new AddObjectCommand(groupJ1));
			
			//console.log("part 1 imprted");
			// document.querySelector('h1').style.display = 'none';

		});
		groupJ2.position.y = .82;
		var part2;
		loader.load('http://127.0.0.1:8080/large_module_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = .75;
			object.position.y = 0;
			object.position.z = 0;
			object.rotateX(THREE.Math.degToRad(0));
			object.rotateY(THREE.Math.degToRad(90));
			object.rotateZ(THREE.Math.degToRad(-90));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);
			part2 = object;

			groupJ2.add(part2);

			part2.name = 'part2';
			groupJ2.name = 'joint2';
			//editor.execute(new AddObjectCommand(groupJ2));
			groupJ1.add(groupJ2);
			//console.log("joint2 parent: ", groupJ2.parent.name);
			// document.querySelector('h1').style.display = 'none';

		});
		var part3;
		loader.load('http://127.0.0.1:8080/arm_tube_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = 1.56;
			object.position.y = 3.62;
			object.position.z = 0;
			object.rotateX(THREE.Math.degToRad(90));
			object.rotateY(THREE.Math.degToRad(0));
			object.rotateZ(THREE.Math.degToRad(0));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);
			part3 = object;
			part3.name = 'part3';
			groupJ2.add(part3);
			//editor.execute(new AddObjectCommand(part3));


			//console.log("part 2 imprted");
			// document.querySelector('h1').style.display = 'none';

		});
		var part4;
		loader.load('http://127.0.0.1:8080/large_module_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = .76;
			object.position.y = 4.26;
			object.position.z = 0;
			object.rotateX(THREE.Math.degToRad(90));
			object.rotateY(THREE.Math.degToRad(90));
			object.rotateZ(THREE.Math.degToRad(0));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);
			part4 = object;
			part4.name = 'part4';
			//editor.execute(new AddObjectCommand(part4));
			groupJ2.add(part4);
			//groupJ2.add(groupJ3);
			//console.log("part 2 imprted");
			// document.querySelector('h1').style.display = 'none';

		});
		groupJ3.position.x = .75;
		groupJ3.position.y = 4.3;
		var part5;
		loader.load('http://127.0.0.1:8080/straight_tube_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = 0;
			object.position.y = 0;
			object.position.z = 0;
			object.rotateX(THREE.Math.degToRad(90));
			object.rotateY(THREE.Math.degToRad(180));
			object.rotateZ(THREE.Math.degToRad(0));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);
			part5 = object;
			part5.name = 'part5';


			groupJ3.name = 'joint3';

			//editor.execute(new AddObjectCommand(groupJ3));

			groupJ3.add(part5);
			groupJ2.add(groupJ3);

			// document.querySelector('h1').style.display = 'none';

		});
		var part6;
		loader.load('http://127.0.0.1:8080/little_module_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = 0.02;
			object.position.y = 3.28;
			object.position.z = 0;
			object.rotateX(THREE.Math.degToRad(0));
			object.rotateY(THREE.Math.degToRad(270));
			object.rotateZ(THREE.Math.degToRad(90));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);
			part6 = object;
			part6.name = 'part6';
			groupJ3.add(part6);
			//console.log("j3 parent",groupJ3.parent.name,"   ended");
			//editor.execute(new AddObjectCommand(groupJ3));
			//console.log("part 2 imprted");
			// document.querySelector('h1').style.display = 'none';

		});
		groupJ4.position.y = 3.28;
		groupJ4.position.x = 0.02;
		var part7;
		loader.load('http://127.0.0.1:8080/little_module_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = .5;
			object.position.y = .68;
			object.position.z = 0;
			object.rotation.x = (THREE.Math.degToRad(270));
			object.rotation.z = (THREE.Math.degToRad(180));

			object.scale.set(0.1, 0.1, 0.1);

			part7 = object;
			//scene.add(part7);
			part7.name = 'part7';
			groupJ4.add(part7);
			groupJ4.name = 'joint4';
			//editor.execute(new AddObjectCommand(groupJ4));
			groupJ3.add(groupJ4);
			//console.log("part 2 imprted");
			// document.querySelector('h1').style.display = 'none';




			groupJ2.position.x = groupJ2.position.x - .1;
			//groupJ2.rotation.x = THREE.Math.degToRad(45);
			//console.log("considered");
		});
		
		groupJ5.position.x = .47;
		groupJ5.position.y = 1.15;
		var part8;
		loader.load('http://127.0.0.1:8080/little_module_2.obj', function (object) {

			//object.material = material;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = materialArm;

				}
			});
			object.rotation.z = Math.PI;
			object.position.x = 0.7;
			object.position.y = 0;
			object.position.z = 0;
			//object.rotateX(THREE.Math.degToRad(0));
			object.rotation.y = (THREE.Math.degToRad(90));
			object.rotation.z = (THREE.Math.degToRad(270));
			object.scale.set(0.1, 0.1, 0.1);
			//scene.add(object);
			part8 = object;
			part8.name = 'part8';
			groupJ5.add(part8);
			groupJ5.name = 'joint5';
			//editor.execute(new AddObjectCommand(groupJ5));
			groupJ4.add(groupJ5);
			//console.log("j4 parent name:  ",groupJ4.parent.name);
			//console.log("j5 parent name:  ",groupJ5.parent.name);
			// document.querySelector('h1').style.display = 'none';

		});
		
		
		groupJ6.position.x = .43;
		var part9;
		loader.load('http://127.0.0.1:8080/terminal_unit.obj', function (object) {
			material = new THREE.MeshPhongMaterial({ color: "rgb(45,45,45)" });
		material.transparent = true;
		material.opacity = 1;
			object.traverse(function (child) {
				if (child instanceof THREE.Mesh) {
					child.material = material;
				}
			});

			object.rotation.z = Math.PI;
			object.position.x = 0.27;
			object.position.y = 0;
			object.position.z = 0;
			//object.rotateX(THREE.Math.degToRad(0));

			object.scale.set(0.01, 0.01, 0.01);
			//scene.add(object);
			part9 = object;
			part9.rotation.x = THREE.Math.degToRad(180);
			part9.rotation.y = THREE.Math.degToRad(180);
			part9.name = 'part9';
			groupJ6.add(part9);
			groupJ6.name = 'joint6';
			//editor.execute(new AddObjectCommand(groupJ6));
			groupJ5.add(groupJ6);
			
			
		}
		);

	}

	setTimeout(() => {
		//editor.execute(new AddObjectCommand(groupJ1));
		editor.sceneHelpers.add(groupJ1);
		//editor.signals.objectChanged.dispatch(groupJ1);
	}, 500);

	//Lines 
	//var currentLine;
	function addLines(newPoint, prevpoint) {
		var material = new THREE.LineBasicMaterial({
			color: 0x0000ff
		});

		var geometry = new THREE.Geometry();
		geometry.vertices.push(
			newPoint,
			prevpoint
		);

		var line = new THREE.Line(geometry, material);
		line.name = 'line' + parseInt(Math.random()*50);
		
		editor.scene.add(line);
		//editor.execute(new AddObjectCommand(line))
		return line;
	}
	//addLines(new THREE.Vector3(0,0,0),new THREE.Vector3(70,10,0));
	instantiateRobotArmComponenst();

	document.addEventListener('keydown', function (event) {
		if (event.keyCode == 78) {
			addTetrahedron();
		}
		else if (event.keyCode == 39) {
			//alert('Right was pressed');
		}
	});

	signals.objectAdded.add(function (object) {
		if (object.name.substring(0, 5) == 'Tetra') {
			//console.log('waypoint added' + previousPoint);
			//start point
			if (previousPoint == null) {
				//console.log('undefined previous point');
				object.previousPoint = previousPoint;
				object.nextPoint = null;
				object.material.color = new THREE.Color('skyblue');
				object.line.push(null);
				//console.log(object);
				previousPoint = object;


				return;
			}
			//rest of the points
			else {
				
				line = addLines(object.position, previousPoint.position);
				
				//console.log( object.position,previousPoint.position);
				object.line[0] = line;
				console.log(object.line);
				object.previousPoint = previousPoint;
				object.nextPoint = null;
				object.previousPoint.nextPoint = object;
				
				object.previousPoint.line[1] = line;
				
				//console.log(object);
				previousPoint = object;
				//console.log(object.previousPoint);
			}
			
		}

	});

	// Update line for waypoint
	signals.objectChanged.add(function (object) {
		//Other objects except waypoint does not have lines
		if (object.name.substring(0, 5) != 'Tetra')
			return;

		///A pont's second element connects with next points first element. They have two elwments[array]
		///Exception is for the very first point 

		if (object.line[0] != null) {
			editor.scene.remove(object.line[0]);
			editor.scene.remove(object.previousPoint.line[1]);
			// editor.execute(new RemoveObjectCommand(object.line[0]));
			// editor.execute(new RemoveObjectCommand(object.previousPoint.line[1]));
			line = addLines(object.position, object.previousPoint.position);
			object.line[0] = line;
		}
		if (object.line[1] != null) {
			editor.scene.remove(object.line[1]);
			editor.scene.remove(object.nextPoint.line[0]);
			// editor.execute(new RemoveObjectCommand(object.line[1]));
			// editor.execute(new RemoveObjectCommand(object.line[1]));
			
			line = addLines(object.position, object.nextPoint.position);
			object.line[1] = line;
		}
		//editor.execute(new RemoveObjectCommand(object.line));
	});
	signals.objectRemoved.add(function (object) {
		if (object.name.substring(0, 5) != 'Tetra')
			return;
		//	
		if(editor.waypoints.includes(object)){
			editor.waypoints.splice(editor.waypoints.indexOf(object),1,);
				
			//start point
			if(object.line[0] == null & object.line[1] != null){
				//remove next line
				editor.scene.remove(object.line[1]);
				editor.scene.remove(object.nextPoint.line[0]);

				//update  points 
				object.nextPoint.previousPoint = null;
				object.nextPoint.line[0] = null;
				
				//update next point as the start point
				object.nextPoint.material = object.material;
				

				
			}
			//middle point(s)
			else if(object.line[0] != null & object.line[1] != null){
				//remove previous line
				editor.scene.remove(object.line[0]);
				editor.scene.remove(object.previousPoint.line[1]);

				//remove next line
				editor.scene.remove(object.line[1]);
				editor.scene.remove(object.nextPoint.line[0]);

				//update previous points next point and vice versa
				object.previousPoint.nextPoint = object.nextPoint;
				object.nextPoint.previousPoint = object.previousPoint;

				//create new line for the updated points
				line = addLines(object.previousPoint.position, object.nextPoint.position);
				object.previousPoint.line[1] = line;
				object.nextPoint.line[0] = line;
			}

			//end point
			else if(object.line[0] != null & object.line[1] == null){
				//remove previous line
				editor.scene.remove(object.line[0]);
				editor.scene.remove(object.previousPoint.line[1]);

				//update  points 
				object.previousPoint.nextPoint = null;
				object.previousPoint.line[1] = null;

				previousPoint = object.previousPoint;

				
			}

				
				
				// line = addLines(object.position, object.nextPoint.position);
				// object.line[1] = line;
			
		}
			
	});

	signals.waypointsAdded.add(function(){

		editor.waypoints.forEach(element => {

			// if(element.previousPoint == null)
			// {
			// 	element.line[0] = null;
			// }
			// else if(element.nextPoint == null)
			// {
			// 	element.line[1] = null;
			// }
			// if(element.nextPoint!=null){
			// 	//line = addLines(element.position,element.nextPoint.position);
			// 	//console.log(element.position.x+'  '+element.name+'  '+element.nextPoint.position.x);
			// 	element.line[1] = line;
			// 	element.nextPoint.line[0] = line;
			// }
			
			
			
		});
		//editor..log(editor.waypoints);
	});

	signals.editorCleared.add(function(){
		//addLightStartofScene();
		previousPoint = null;
		signals.refreshRendering.dispatch();
		
	});
	return container;

};
