/**
 * @author mrdoob / http://mrdoob.com/
 */

var Sidebar = function ( editor ) {

	var strings = editor.strings;

	var container = new UI.Panel();
	container.setId( 'sidebar' );

	//

	var sceneTab = new UI.Text( strings.getKey( 'sidebar/scene' ) ).setTextTransform( 'uppercase' );
	sceneTab.onClick( function () { select( 'SCENE' ) } );

	var projectTab = new UI.Text( strings.getKey( 'sidebar/project' ) ).setTextTransform( 'uppercase' );
	projectTab.onClick( function () { select( 'PROJECT' ) } );

	var settingsTab = new UI.Text( strings.getKey( 'sidebar/settings' ) ).setTextTransform( 'uppercase' );
	settingsTab.onClick( function () { select( 'SETTINGS' ) } );

	var tabs = new UI.Div();
	tabs.setId( 'tabs' );
	//tabs.add( sceneTab, projectTab, settingsTab );
	tabs.add(sceneTab);
	container.add( tabs );

	//

	var scene = new UI.Span().add(
		new Sidebar.Scene( editor ),
		new Sidebar.Properties( editor ),
		new Sidebar.Animation( editor )
		//new Sidebar.Script( editor )
	);
	container.add( scene );

	var project = new UI.Span().add(
		new Sidebar.Project( editor )
	);
	//container.add( project );

	var settings = new UI.Span().add(
		new Sidebar.Settings( editor ),
		new Sidebar.History( editor )
	);
	//container.add( settings );
	var horizontalLine = new UI.HorizontalRule()
	container.add(horizontalLine);
	var sceneTab1 = new UI.Text( "JOINT ANGLES").setPaddingLeft('10px').setColor("grey");
	var tabs1 = new UI.Div();
	tabs1.setId( 'tabs1' );
	//tabs.add( sceneTab, projectTab, settingsTab );
	tabs1.add(sceneTab1);
	container.add( tabs1);
	var objectNameRow = new UI.Row().setPaddingTop('20px');
	var objectName = new UI.Input().setWidth( '50px' ).setFontSize( '12px' );

	objectNameRow.add( new UI.Text( "joint1" ).setPaddingLeft("10px").setColor("grey"));
	var objectName2 = new UI.Input().setWidth( '50px' ).setFontSize( '12px' );

	objectName.setValue("0");
	objectNameRow.add( objectName );
	objectNameRow.add( new UI.Text( "joint2" ).setPaddingLeft('10px').setColor("grey"));
	objectNameRow.add( objectName2 );
	objectName2.setValue("0");
	var objectName3 = new UI.Input().setWidth( '50px' ).setFontSize( '12px');
	objectNameRow.add( new UI.Text( "joint3" ).setPaddingLeft('10px').setColor("grey"));
	objectNameRow.add( objectName3 );
	objectName3.setValue("0");
	var objectName4= new UI.Input().setWidth( '50px' ).setFontSize( '12px' ).setMarginTop('5px');
	objectNameRow.add( new UI.Text( "joint4" ).setPaddingLeft('10px').setColor("grey").setMarginTop('5px'));
	objectNameRow.add( objectName4 );
	objectName4.setValue("0");
	var objectName5 = new UI.Input().setWidth( '50px' ).setFontSize( '12px' ).setMarginTop('5px');
	objectNameRow.add( new UI.Text( "joint5" ).setPaddingLeft('10px').setColor("grey").setMarginTop('5px'));
	objectNameRow.add( objectName5 );
	objectName5.setValue("0");
	var objectName6 = new UI.Input().setWidth( '50px' ).setFontSize( '12px' ).setMarginTop('5px');
	objectNameRow.add( new UI.Text( "joint6" ).setPaddingLeft('10px').setColor("grey").setMarginTop('5px'));
	objectNameRow.add( objectName6 );
	objectName6.setValue("0");
	container.add( objectNameRow );
	
	var submit  = new UI.Div();
	var AnimateButton = new UI.Button("Animate").setMarginLeft('110px').onClick(function(){
		//console.log(typeof objectName.dom.value);
		animateArms(parseFloat(objectName.dom.value),parseFloat(objectName2.dom.value),parseFloat(objectName3.dom.value),parseFloat(objectName4.dom.value),parseFloat(objectName5.dom.value),parseFloat(objectName6.dom.value));
	});
	//animateArms(10,20,30,40,50,50);
	setTimeout(() => {
		console.log(editor.groupJ1);
		reRender();
		//animateArms(45,20,30,40,50,50);
		//editor.groupJ1.rotation.y = THREE.Math.degToRad(45);
		
	}, 1500);
	//console.log(Menubar.groupJ1)
	//.addEventListener("click",animateArms(objectName.getValue,objectName2.getValue,objectName3.getValue,objectName4.getValue,objectName4.getValue,objectName5.getValue,objectName6.getValue));
	//AnimateButton.setPaddingTop("30px").setPaddingLeft("40px");
	submit.add(AnimateButton);
	var zerPositionButton = new UI.Button("Zero Pose").setMarginRight('0px').setMarginLeft('40px').onClick(function(){
		animateArms(0,0,0,0,0,0);
	});
	submit.add(zerPositionButton);
	submit.setPaddingTop("20px");
	container.add(submit);
	//
	//Function fo showing animation 
function reRender(){
	//editor.newAnimationRequest = !editor.newAnimationRequest;
	editor.signals.refreshRendering.dispatch();
	console.log('rerendering')
}
function animateArms(finalAngle1,finalAngle2,finalAngle3,finalAngle4,finalAngle5,finalAngle6,timeStep = 3000) {

	console.log(parseInt(finalAngle1), finalAngle2, finalAngle3, finalAngle4, finalAngle5, finalAngle6);
	var currentAngle1 = editor.groupJ1.rotation.y;
	var currentAngle2 = editor.groupJ2.rotation.x;
	var currentAngle3 = editor.groupJ3.rotation.x;
	var currentAngle4 = editor.groupJ4.rotation.x;
	var currentAngle5 = editor.groupJ5.rotation.y;
	var currentAngle6 = editor.groupJ6.rotation.x;
	//requestAnimationFrame(reRender);
	var refreshRender = setInterval(reRender,50);

	setTimeout(()=>{clearInterval(refreshRender)},3000);

	
	//k = THREE.Math.degToRad(k);
    tweenCustomVar(90, 90, {
        variable: 'loadingPercentage', // the variable we want to tween
        duration: timeStep,
        easing: TWEEN.Easing.Linear.None,
        update: function (d) {
            //console.log("Updating: " + d*57.29);
			editor.groupJ1.rotation.y = (d * (parseFloat(finalAngle1)-THREE.Math.radToDeg(currentAngle1)) / 57.29) + currentAngle1;
        },

        callback: function () {
            // console.log("Completed");
        }
    });
    tweenCustomVar(90, 90, {
        variable: 'loadingPercentage', // the variable we want to tween
        duration: timeStep,
        easing: TWEEN.Easing.Linear.None,
        update: function (d) {
            //console.log("Updating: " + d*57.29);
			editor.groupJ2.rotation.x= (d * (parseFloat(finalAngle2)-THREE.Math.radToDeg(currentAngle2)) / 57.29) + currentAngle2;
        },

        callback: function () {
            // console.log("Completed");
        }
    });
    tweenCustomVar(90, 90, {
        variable: 'loadingPercentage', // the variable we want to tween
        duration: timeStep,
        easing: TWEEN.Easing.Linear.None,
        update: function (d) {
			//k = k + d;
            //console.log("Total Rotation: " + "   "+ THREE.Math.radToDeg(-d * (parseFloat(finalAngle3)+THREE.Math.radToDeg(k)) / 57.29));
			editor.groupJ3.rotation.x = (-d * (parseFloat(finalAngle3)+THREE.Math.radToDeg(currentAngle3)) / 57.29) + currentAngle3;
        },

        callback: function () {
            // console.log("Completed");
        }
    });
    tweenCustomVar(90, 90, {
        variable: 'loadingPercentage', // the variable we want to tween
        duration: timeStep,
        easing: TWEEN.Easing.Linear.None,
        update: function (d) {
           // console.log("Updating: " + -d*57.29);
			editor.groupJ4.rotation.x = (d * (parseFloat(finalAngle4)-THREE.Math.radToDeg(currentAngle4)) / 57.29) + currentAngle4;
			//requestAnimationFrame(reRender);
			//reRender();
        },

        callback: function () {
            // console.log("Completed");
        }
    });
    tweenCustomVar(90, 90, {
        variable: 'loadingPercentage', // the variable we want to tween
        duration: timeStep,
        easing: TWEEN.Easing.Linear.None,
        update: function (d) {
            //console.log("Updating: " + d*57.29);
			editor.groupJ5.rotation.y = (d * (parseFloat(finalAngle5)-THREE.Math.radToDeg(currentAngle5)) / 57.29) + currentAngle5;
        },

        callback: function () {
            // console.log("Completed");
        }
    });
    tweenCustomVar(90, 90, {
        variable: 'loadingPercentage', // the variable we want to tween
        duration: timeStep,
        easing: TWEEN.Easing.Linear.None,
        update: function (d) {
            //console.log("Updating: " + d*57.29);
			editor.groupJ6.rotation.x = (d * (parseFloat(finalAngle6)- THREE.Math.radToDeg(currentAngle6))/ 57.29) + currentAngle6;
        },

        callback: function () {
            // console.log("Completed");
        }
    });
}
function tweenCustomVar(obj, target, options) {
    options = options || {};
    var easing = options.easing || TWEEN.Easing.Linear.None,
        duration = options.duration || timeStep,
        variable = options.variable || 'opacity',
        tweenTo = {};
    tweenTo[variable] = target; // set the custom variable to the target
    var tween = new TWEEN.Tween(obj)
        .to(tweenTo, duration)
        .easing(easing)
        .onUpdate(function (d) {
            if (options.update) {
                options.update(d);
            }
        })
        .onComplete(function () {
            if (options.callback) {
                options.callback();
            }
        });
    tween.start();
    animate();

    function animate() {
        requestAnimationFrame(animate);
        // [...]
        TWEEN.update();
        // [...]
    }
    return tween;
}

var horizontalLine = new UI.HorizontalRule();
container.add(horizontalLine);

var connectionText = new UI.Text('connected').setColor('grey').setMarginLeft('10px');
container.add(connectionText);

var connectionStatus = new UI.Checkbox();
//connectionStatus.setValue(true);
container.add(connectionStatus);
//Ros initialise 
var ros = new ROSLIB.Ros({
    url : 'ws://10.2.0.79:9090'
  });

  ros.on('connection', function() {
	console.log('Connected to websocket server.');
	connectionStatus.setValue(true);
  });

  ros.on('error', function(error) {
	console.log('Error connecting to websocket server: ', error);
	
  });

  ros.on('close', function() {
	console.log('Connection to websocket server closed.');
	connectionStatus.setValue(false);
  });

  // Publishing a Topic
  // ------------------

  var cmdVel = new ROSLIB.Topic({
    ros : ros,
    name : '/cmd_vel',
    messageType : 'geometry_msgs/Twist'
  });

  var twist = new ROSLIB.Message({
    linear : {
      x : 0.1,
      y : 0.2,
      z : 0.3
    },
    angular : {
      x : -0.1,
      y : -0.2,
      z : -0.3
    }
  });
  

  // Subscribing to a Topic
  // ----------------------

  var listener = new ROSLIB.Topic({
    ros : ros,
    name : '/joint_states',
    messageType : 'sensor_msgs/JointState'
  });

  listener.subscribe(function(message) {
	console.log('Received message on ' + listener.name + ': ' + message.position[2]);
	var i = editor.groupJ1.rotation.y;
	var j = editor.groupJ2.rotation.x;
	var k = editor.groupJ3.rotation.x;
	var l = editor.groupJ4.rotation.x;
	var m = editor.groupJ5.rotation.y;
	var n = editor.groupJ6.rotation.x;
	editor.groupJ1.rotation.y = -message.position[0];
	editor.groupJ2.rotation.x = -message.position[1];
	editor.groupJ3.rotation.x = message.position[2];
	editor.groupJ4.rotation.x = -message.position[3];
	editor.groupJ5.rotation.y = -message.position[4];
	editor.groupJ6.rotation.x = -message.position[5];
	//(-message.position[0]*57.3,-message.position[1]*57.3,-message.position[2]*57.3,-message.position[3]*57.3,-message.position[4]*57.3,-message.position[5]*57.3,20);
	editor.signals.refreshRendering.dispatch();
   // listener.unsubscribe();
  });

  // Calling a service
  // -----------------

//   var addTwoIntsClient = new ROSLIB.Service({
//     ros : ros,
//     name : '/add_two_ints',
//     serviceType : 'rospy_tutorials/AddTwoInts'
//   });

//   var request = new ROSLIB.ServiceRequest({
//     a : 1,
//     b : 2
//   });

//   addTwoIntsClient.callService(request, function(result) {
//     console.log('Result for service call on '
//       + addTwoIntsClient.name
//       + ': '
//       + result.sum);
//   });

  // Getting and setting a param value
  // ---------------------------------

  ros.getParams(function(params) {
    console.log(params);
  });

  var maxVelX = new ROSLIB.Param({
    ros : ros,
    name : 'max_vel_y'
  });

  maxVelX.set(0.8);
  maxVelX.get(function(value) {
    console.log('MAX VAL: ' + value);
  });
var submit  = new UI.Div();
	var sendPoseButton = new UI.Button("Send Pose").setMarginTop('50px').onClick(function(){
		//console.log(typeof objectName.dom.value);
		console.log('info sent');
		waypoinfinalAngle1 = editor.scene.getObjectByName('Tetrahedron1');
		if(waypoinfinalAngle1 == null)
			console.log('waypoint does not exist');
		else {
			console.log(waypoinfinalAngle1.position);
			twist = new ROSLIB.Message({
				linear : {
				  x : -waypoinfinalAngle1.position.z/10,
				  y : -waypoinfinalAngle1.position.x/10,
				  z : waypoinfinalAngle1.position.y/10
				},
				angular : {
				  x : waypoinfinalAngle1.rotation.x,
				  y : waypoinfinalAngle1.rotation.z,
				  z : waypoinfinalAngle1.rotation.y
				}
			  });
			  cmdVel.publish(twist);
		}
		//send information
	});
	sendPoseButton.setMarginLeft("60px");
	submit.add(sendPoseButton);
	
	//submit.setPaddingTop("20px").setPaddingLeft("100px")
	container.add(submit);

	function select( section ) {

		sceneTab.setClass( '' );
		projectTab.setClass( '' );
		settingsTab.setClass( '' );

		scene.setDisplay( 'none' );
		project.setDisplay( 'none' );
		settings.setDisplay( 'none' );

		switch ( section ) {
			case 'SCENE':
				sceneTab.setClass( 'selected' );
				scene.setDisplay( '' );
				break;
			case 'PROJECT':
				projectTab.setClass( 'selected' );
				project.setDisplay( '' );
				break;
			case 'SETTINGS':
				settingsTab.setClass( 'selected' );
				settings.setDisplay( '' );
				break;
		}

	}

	select( 'SCENE' );

	return container;

};
